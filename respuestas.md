Cada pregunta viene seguida de la respuesta con una tabulación más.
## Ejercicio 3. Análisis general

* ¿Cuántos paquetes componen la captura?
  * 1090 paquetes.
* ¿Cuánto tiempo dura la captura?
  * 14 segundos y medio.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
  * La IP del usuario es: 192.168.1.34. Es una IP privada ya que se compone del rango privado en IPv4 con el rango de direcciones 192.168.0.0/16. Este rango se corresponde con la clase C de las direcciones privadas. 

Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de `Statistics`. En el apartado de jerarquía de protocolos (`Protocol Hierarchy`) se puede ver el número de paquetes y el tráfico correspondiente a los distintos protocolos.

* ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
  * El principal protocolo en el nivel de aplicación es RTP (Real-Time Transport Protocol) que se corresponde con un 96,2% (1049 paquetes) de toda la trama. Seguido de este, los paquetes SIP (Session Initiation Protocol) forman un 0,8% (9 paquetes) de toda la trama. Y por último, con un 0,2%, hay 2 paquetes STUN (Session Traversal Utilities for NAT).
* ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
  * Toda la trama viaja por el protocolo Ethernet que se corresponde con la capa de Red. En segundo lugar, toda la trama esta compuesta por paquetes IP en la capa de Internet, aunque  también hay 3 paquetes ICMP (Internet Control Message Protocol). El resto de paquetes que no son ICMP, ya se corresponden con la capa de transporte por UDP (User Datagram Protocol).
* ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
  * RTP con 180.428 bytes enviados correspondiendo con el 96,2% de toda la trama. La tasa es 12,4 Mbytes/s. 

Observa por encima el flujo de tramas en el menú de `Statistics` en `IO Graphs`. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, incluyendo una llamada. 

* Filtra por `sip` para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
  * En el segundo 0 se envian 4 paquetes. En el segundo 3 se envian otros 3 paquetes. Y por último en el segundo 14 se envían 2 paquetes. 
* Y los paquetes con RTP, ¿cuándo se empiezan a enviar?
  * A partir del paquete 12, en el 3,97 segundos.
* Los paquetes RTP, ¿cuándo se dejan de enviar?
  * El ultimo paquete RTP es el paquete 1088 en el segundo 14,48.
* Los paquetes RTP, ¿cada cuánto se envían?
  * Por cada centésima de segundo, se envia un paquete RTP.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 4. Primeras tramas

Analiza ahora las dos primeras tramas de la captura. Cuando se pregunta por máquinas, llama "Linphone" a la máquina donde está funcionando el cliente SIP, y "Servidor" a la máquina que proporciona el servicio SIP.

* ¿De qué protocolo de nivel de aplicación son?
  * Son paquetes SIP.
* ¿Cuál es la dirección IP de la máquina "Linphone"?
  * 192.168.1.34.
* ¿Cuál es la dirección IP de la máquina "Servidor"?
  * 212.79.111.155.
* ¿En qué máquina está el UA (user agent)?
  * En la máquina con la IP 192.168.1.34.
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la máquina con IP 192.168.1.34 a la máquina 212.79.111.155. Y la segunda trama es justo al revés, es decir, el servidor contesta.
* ¿Que ha ocurrido para que se envíe la primera trama?
  * La máquina envía una petición REGISTER al servidor.
* ¿Qué ha ocurrido para que se envíe la segunda trama?
  * El servidor responde este REGISTER con un 401 Unauthorized porque el Nonce Value no está firmado por el usuario.

Ahora, veamos las dos tramas siguientes.

* ¿De qué protocolo de nivel de aplicación son?
  * Son paquetes SIP.
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la máquina con IP 192.168.1.34 a la máquina 212.79.111.155. Y la segunda trama es justo al revés, es decir, el servidor contesta.
* ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
  * Esta trama se corresponde con el segundo REGISTER que envía la máquina ya con la autentificación firmada para que el servidor vea que de verdad es ese usuario y no otro.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
  * El servidor responde un 200 OK una vez firmada la autentificación, es decir, estos paquetes son necesarios para empezar la sesión de envío de datos RTP.

Ahora, las tramas 5 y 6.

* ¿De qué protocolo de nivel de aplicación son?
  * Son paquetes SIP.
* ¿Entre qué máquinas se envía cada trama?
  * La primera trama se envía desde la máquina con IP 192.168.1.34 a la máquina 212.79.111.155. Y la segunda trama es justo al revés, es decir, el servidor contesta.
* ¿Que ha ocurrido para que se envíe la primera de ellas (quinta trama en la captura)?
  * El usuario envia una petición INVITE bajo SIP/SDP para verificar los protocolos, estándares y normas que van a intervenir para hacer el intercambio de datos de audio por RTP.
* ¿Qué ha ocurrido para que se envíe la segunda de ellas (sexta trama en la captura)?
  * El servidor contesta con un 200 OK con las características de la sesión RTP futura.
* ¿Qué se indica (en líneas generales) en la parte SDP de cada una de ellas?
  * En el primer paquete SDP se envía el nombre el usuario, en este caso jgbarah, con su IP: 192.168.1.34. En el apartado de Media Desciption es donde están todos los parametros de la sesión RTP:
    * Se va a enviar audio.
    * En el puerto 7078.
    * Por el protocolo RTP.
    * Bajo el estándar ITU-T G.711.
    * Acepta Dynamic-RTP
    * El codec de audio digital es Opus.
    * Y ya otros valores como el sample rate o frecuencia de muestreo a la que va a ir codificado el audio.
  * En el segundo paquete SDP se envía el ID de la sesión que es 1902109695, la IP del servidor que es la 212.79.111.155. Y en el apartado de Media Description aparece la misma información que en el anterior paquete confirmando los parametros RTP y de audio.

Después de la trama 6, busca la primera trama SIP.

* ¿Qué trama es?
  * Es la respuesta ACK.
* ¿De qué máquina a qué máquina va?
  * De la máquina 192.168.1.34 al servidor con la IP 212.79.111.155.
* ¿Para qué sirve?
  * Sirve para decirle al servidor que acepta esas condiciones para la sesión RTP, es decir, que se van a utilizar esos formatos, codecs, estándares, etc.
* ¿Puedes localizar en ella qué versión de Linphone se está usando?
  * Linphone/3.12.0 (belle-sip/1.6.3.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 5. Tramas especiales

Las tramas 7 y 9 parecen un poco especiales:

* ¿De que protocolos son (indica todos los protocolos relevantes por encima de IP).
  * UDP y STUN.
* De qué máquina a qué máquina van?
  * De la máquina con la IP 192.168.1.34 al servidor con la IP 212.79.111.155.
* ¿Para qué crees que sirven?
  * STUN sirve para que el servidor sepa que IP pública tiene el cliente que solicita la comunicación UDP y que puerto está disponible para dicha comunicación para permitir el tráfico entrante a la red del cliente.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 6. Registro

Repasemos ahora qué está ocurriendo (en lo que al protocolo SIP se refiere) en las primeras tramas SIP que hemos visto ya:

* ¿Qué dirección IP tiene el servidor que actúa como registrar SIP? ¿Por qué?
  * 212.79.111.155. Es una IP pública ya que el servidor esta en algún lugar de internet y necesitamos acceder a ella para conectarnos ya que aquí está el servidor SIP.
* ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
  * El 5060 que es el puerto por defecto.
* ¿Qué método SIP utiliza el UA para registrarse?
  * El método REGISTER.
* ¿Qué diferencia fundamental ves entre la primera línea del paquete SIP que envía Linphone para registrar a un usuario, y el que hemos estado enviando en la práctica 4?
  * Aquí el Request-Line es: REGISTER sip:iptel.org SIP/2.0, es decir, no viene la IP ni el puerto ya que esa información viene en las cabeceras de SIP. La IP y el puerto del usuario que se quiere registrar si teniamos que ponerlo en la práctica 4.
* ¿Por qué tenemos dos paquetes `REGISTER` en la traza?
  * Porque la primera petición es para que el servidor mande una encriptación que tendrá que ser firmada por el usuario en el segundo REGISTER para que el servidor sepa que ese usuario es justo ese usuario y no otro. Es un mecanismo de seguridad. En el segundo REGISTER el cliente ya envía un Digest Authentication Response que se corresponde con su firma. Y ahí ya el servidor le deja registrarse y le responde con un 200 OK.
* ¿Por qué la cabecera `Contact` es diferente en los paquetes 1 y 3? ¿Cuál de las dos cabeceras es más "correcta", si nuestro interés es que el UA sea localizable?
  * Por que tenemos dos IP's distintas. La que realiza el primer REGISTER es la 192.168.1.34 que es la IP privada de esa red. Y en el segundo REGISTER la realiza la IP 83.38.204.116 que es la IP pública que se corresponde con esa red ya que está en su lista NAT. La cabecera que se corresponde con la IP pública es la más correcta ya que solo existe esa IP en todo el mundo y el tráfico va a ser solo con esa IP, además, la NAT se encarga de dirigir el tráfico hacía la máquina en concreto dentro de esa red privada que es la 192.168.1.34.
* ¿Qué tiempo de validez se está dando para el registro que pretenden realizar los paquetes 1 y 3?
  * 3600 segundos.

Para responder estas preguntas, puede serte util leer primero [Understanding REGISTER Method](https://blog.wildix.com/understanding-register-method/), además de tener presente lo explicado en clase.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 7. Indicación de comienzo de conversación

* Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
  * INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY, MESSAGE, SUBSCRIBE, INFO, UPDATE.
* ¿En qué paquete el UA indica que quiere comenzar a intercambiar datos de audio con otro? ¿Qué método SIP utiliza para ello?
  * En el paquete 5 que es el INVITE.
* ¿Con qué dirección quiere intercambiar datos de audio el UA?
  * Con el servidor que tiene la dirección 212.79.111.155.
* ¿Qué protocolo (formato) está usando para indicar cómo quiere que sea la conversación?
  * SDP que es donde viene toda la información de los contenidos multimedia que se van a transmitir.
* En la indicacion de cómo quiere que sea la conversación, puede verse un campo `m` con un valor que empieza por `audio 7078`. ¿Qué indica el `7078`? ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Es el puerto que se habilita para que el cliente envie los datos de audio. Todos los paquetes RTP de audio que envie el cliente llevarán este puerto ya que es el puerto por defecto de RTP.
* En la respuesta a esta indicacion vemos un campo `m` con un valor que empieza por `audio 27138`. ¿Qué indica el `27138`?  ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama? ¿Qué paquetes son esos?
  * Es el puerto que se habilita para que el servidor envie los datos de audio. Todos los paquetes RTP de audio que envie el servidor llevarán este puerto.
* ¿Para qué sirve el paquete 11 de la trama?
  * Es un ACK que envia el UA para confirmar la validación de todos los parámetros de la conexión. Una vez enviado este ACK y ya las dos máquinas se han puesto de acuerdo en todos los parámetros y ya conocen la IP pública del cliente y los puertos y demás, ya empieza la comunicación RTP.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 8. Primeros paquetes RTP

Vamos ahora a centrarnos en los paquetes RTP. Empecemos por el paquete 12 de la traza:

* ¿De qué máquina a qué máquina va?
  * De la máquina con la IP 192.168.1.34 a la 212.79.111.155.
* ¿Qué tipo de datos transporta?
  * Transporta audio codificado en ITU-T G.711 PCMU.
* ¿Qué tamaño tiene?
  * 1712 bits (214 bytes).
* ¿Cuántos bits van en la "carga de pago" (payload)?
  * 160 bytes. Se puede saber si seleccionas payload y lo miras en el apartado de bytes.
* ¿Se utilizan bits de padding?
  * No, esta a 0.
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * Aproximadamente dos centésimas de segundo que son 20ms.
* ¿Cuántos bits/segundo se envían?
  * 99 kbits/s.

Y ahora, las mismas preguntas para el paquete 14 de la traza:

* ¿De qué máquina a qué máquina va?
  * De la máquina con la IP 192.168.1.34 a la 212.79.111.155.
* ¿Qué tipo de datos transporta?
  * Transporta audio codificado en ITU-T G.711 PCMU.
* ¿Qué tamaño tiene?
  * 1712 bits (214 bytes).
* ¿Cuántos bits van en la "carga de pago" (payload).
  * 160 bytes.
* ¿Se utilizan bits de padding?
  * No, esta a 0.
* ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
  * 20 ms.
* ¿Cuántos bits/segundo se envían?
  * 99 kbits/s.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 9. Flujos RTP

Vamos a ver más a fondo el intercambio RTP. Busca en el menú `Telephony` la opción `RTP`. Empecemos mirando los flujos RTP.

* ¿Cuántos flujos hay? ¿por qué?
  * Hay dos flujos porque ambas máquinas se están enviando información de audio, tanto el UA como el servidor, ya que se corresponde con una llamada.
* ¿Cuántos paquetes se pierden?
  * Ningún paquete se ha perdido en la comunicación.
* Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
  * 30,834 ms.
* Para el flujo desde Servidor hacia LinPhone: ¿cuál es el valor máximo del delta?
  * 59,586 ms.
* ¿Qué es lo que significa el valor de delta?
  * Delta es la diferencia de tiempo que hay entre el paquete actual y el paquete anterior. Si Delta es 0 el paquete actual se ha enviado justo en el momento cuando el flujo de datos lo marca según la frecuencia. Pero como esto no es así nunca debido a retardo de la línea, se produce una variabilidad que puede ser negativa o positiva dependiendo de si el paquete llega antes o después en relación con el anterior.
* ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
  * En el flujo que va desde la máquina con la IP 192.168.1.34 a la máquina con la IP 212.79.111.155.
* ¿Cuáles son esos valores?
  * Max Jitter = 8.45 ms. Mean Jitter = 4.15 ms.
* ¿Qué significan esos valores?
  * Son los ms que se han ido acumulando de retardo de la señal, la variabilidad total que ha sufrido la comunicación respecto a la señal de reloj.
* Dados los valores de jitter que ves, ¿crees que se podría mantener una conversación de calidad?
  * Si, ya que esta por debajo de 20 ms que sería un jitter con un impacto notable en la calidad.

Vamos a ver ahora los valores de un paquete concreto, el paquete 17. Vamos a analizarlo en opción `Stream Analysis`:

* ¿Cuánto valen el delta y el jitter para ese paquete?
  * Delta = 20,58 ms. Jitter = 0,10 ms.
* ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
  * Si, gracias al valor de skew, si fuese muy negativo.
* El "skew" es negativo, ¿qué quiere decir eso?
  * Que ha llegado después de lo que tenía que llegar respecto a la señal de reloj de la comunicación.

Si sigues el jitter, ves que en el paquete 78 llega a ser de 5.52:

* ¿A qué se debe esa subida desde el 0.57 que tenía el paquete 53?
  * Porque en ese tramo de la traza la variabilidad de llegada de los paquetes ha sido muy alta llegando incluso al valor de -10 ms, es decir, retrasandose ese paquete 10 ms de cuand tenia que llegar. Por eso el jitter ha subido considerablemente en este tramo.

En el panel `Stream Analysis` puedes hacer `play` sobre los streams:

* ¿Qué se oye al pulsar `play`?
  * Un solo de guitarra con un cantante al final más ruido de fondo como de un bar.
* ¿Qué se oye si seleccionas un `Jitter Buffer` de 1, y pulsas `play`?
  * El audio se entrecorta debido a la pérdida de paquetes.
* ¿A qué se debe la diferencia?
  * Con un jitter buffer tan pequeño no se puede asegurar la calidad de la señal debido a que los paquetes se pierden por no poder almacenarlos correctamente.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.
  
## Ejercicio 10. Llamadas VoIP

Podemos ver ahora la traza, analizándola como una llamada VoIP completa. Para ello, en el menú `Telephony` seleccina el menú `VoIP calls`, y selecciona la llamada hasta que te salga el panel correspondiente:

* ¿Cuánto dura la llamada?
  * 14,49 segundos.

Ahora, pulsa sobre `Flow Sequence`:

* Guarda el diagrama en un fichero (formato PNG) con el nombre `diagrama.png`.
* ¿En qué segundo se realiza el `INVITE` que inicia la conversación?
  * En el 3,87s.
* ¿En qué segundo se recibe el último OK que marca su final?
  * En el 14,49s.
  
Ahora, pulsa sobre `Play Sterams`, y volvemos a ver el panel para ver la transmisión de datos de la llamada (protocolo RTP):

* ¿Cuáles son las SSRC que intervienen?
  * 0x761f98b2 y 0xeeccf15f.
* ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
  * 524 paquetes.
* ¿Cuántos en el sentido contrario?
  * 525 paquetes.
* ¿Cuál es la frecuencia de muestreo del audio?
  * 8000 Hz.
* ¿Qué formato se usa para los paquetes de audio?
  * Se utiliza el codec G.711.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## Ejercicio 11. Captura de una llamada VoIP

Dirígete a la [web de LinPhone](https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
Lanza LinPhone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú `Ayuda` y seleccionar `Asistente de Configuración de Cuenta`. Al terminar, cierra completamente LinPhone.

* Captura una llamada VoIP con el identificador SIP `sip:music@sip.iptel.org`, de unos 15 segundos de duración. Recuerda que has de comenzar a capturar tramas antes de arrancar LinPhone para ver todo el proceso. Guarda la captura en el fichero `linphone-music.pcapng`. Procura que en la captura haya sólo paquetes entre la máquina donde has hecho la captura y has ejecutado LinPhone, y la máquina o máquinas que han intervenido en los intercambios SIP y RTP con ella.
  * Hay que filtrar la trama con: "sip || rtp" para ver solo los paquetes que nos interesan.
* ¿Cuántos flujos tiene esta captura?
  * Dos flujos. Desde la máquina con la IP 212.79.111.155 a la máquina con la IP 212.128.255.44 y viceversa,
* ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de cada uno de los flujos?
  * Flujo desde 212.79.111.155 a 212.128.255.44:
    * Valor máximo de Delta = 23,137 ms. Max Jitter = 0,704 ms. Mean Jitter = 0,303 ms.
  * Flujo desde 212.128.255.44 a 212.79.111.155:
    * Valor máximo de Delta = 32,926 ms. Max Jitter = 5,918 ms. Mean Jitter = 5,347 ms.

Sigue respondiendo las cuestiones que se plantean en este guión en el fichero `respuesta.md` (respondiendo en una nueva sección de respuesta, como se ha indicado).
Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:

* Se valorará que haya realizado al menos haya ocho commits, correspondientes más o menos con los ejercicios pedidos, en al menos dos días diferentes, sobre la rama principal del repositorio.
* Se valorará que el fichero de respuestas esté en formato Markdown correcto.
* Se valorará que esté el texto de todas las preguntas en el fichero de respuestas, tal y como se indica al principio de este enunciado.
* Se valorará que las respuestas sean fáciles de entender, y estén correctamente relacionadas con las preguntas.
* Se valorará que la captura que se pide tenga exactamente lo que se pide.
* Se valorará que el fichero con el diagrama de la llamada VoIP tenga solo ese diagrama, en el formato que se indica en el enunciado.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para los archivos son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Cuando tengas la práctica lista, puedes realizar una prueba general, de que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Ejercicio 12 (segundo periodo). Llamada LinPhone

Ponte de acuerdo con algún compañero para realizar una llamada SIP conjunta (entre los dos) usando LinPhone, de unos 15 segundos de duración.  Si quieres, usa el foro de la asignatura, en el hilo correspondiente a este ejercicio, para enconrar con quién hacer la llamada.

Realiza una captura de esa llamada en la que estén todos los paquetes SIP y RTP (y sólo los paquetes SIP y RTP). No hace falta que tu compañero realice captura (pero puede realizarla si quiere que le sirva también para este ejercicio).

Guarda tu captura en el fichero `captura-a-dos.pcapng`

Como respuesta a este ejercicio, indica con quién has realizado la llamada, quién de los dos la ha iniciado, y cuantos paquetes hay en la captura que has realizado.
* Filtrado por "rtp || sip": He iniciado yo la llamada como: "Reffi" <sip:refferenz@212.128.255.48>; y he llamado a <sip:namengua@212.128.255.50:5060>. Hay 866 paquetes de RTP en un flujo y en el otro 865. Hay 4 flujos RTP, dos de ellos con el codec de audio VP8, y otros dos con el codec opus. 


